package com.example.admin.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.admin.entity.AdminEntity;

public interface AdminRegisterRepositary extends CrudRepository<AdminEntity,String>{

}
