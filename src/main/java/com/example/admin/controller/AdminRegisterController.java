package com.example.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.admin.entity.AdminEntity;
import com.example.admin.service.AdminRegisterService;


@RestController
@RequestMapping(value="/admin")
@EnableHystrix
public class AdminRegisterController {
	
	@Autowired
	AdminRegisterService adminservice;

	@PostMapping(value="/register", produces ="application/json")
	public String registerAdmin(@RequestBody AdminEntity admin) {
		return adminservice.registerAdmin(admin);
		
	}
	
	@GetMapping(value="/userSearch", produces = "application/json")
	@ResponseBody
	public String searchUsers(@RequestParam String jobrole) throws JSONException {
		String users = adminservice.retirveUser(jobrole);
		
		return users;
		
	}
	
}
