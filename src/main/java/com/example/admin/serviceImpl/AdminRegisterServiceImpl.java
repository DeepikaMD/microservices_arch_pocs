package com.example.admin.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Component;

import com.example.admin.entity.AdminEntity;
import com.example.admin.repo.AdminRegisterRepositary;
import com.example.admin.service.AdminRegisterService;
import com.example.admin.util.ServiceToServiceCall;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class AdminRegisterServiceImpl implements AdminRegisterService {
	
	@Autowired
	AdminRegisterRepositary adminrepo;
	
	

	public String registerAdmin(AdminEntity admin) {
		adminrepo.save(admin);
		return "success";
	}

@HystrixCommand(fallbackMethod = "callFallbackMethod")	
	public String retirveUser(String jobrole) throws JSONException {
		
		ServiceToServiceCall serviceObj = new ServiceToServiceCall();
		String response = serviceObj.callService(jobrole);	
		return response;
	}

public String callFallbackMethod(String jobrole) {
	
	System.out.println("inside fallback");
	return "Looks like Service is down!";
	
}
	
	
}
