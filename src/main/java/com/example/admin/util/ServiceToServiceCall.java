package com.example.admin.util;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ServiceToServiceCall {
	/*
	 * @Autowired LoadProperty preload;
	 */

	public String callService(String jobrole){
		String response = null;
		String url = "http://localhost:8081/user/user/retriveAllUsers";
			//String url = "http://10.0.75.2:8080/user/user/retriveAllUsers";
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("jobrole", jobrole);
			HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			ResponseEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
					String.class);
			
				response = result.getBody();
				return response;
				
	}

}
