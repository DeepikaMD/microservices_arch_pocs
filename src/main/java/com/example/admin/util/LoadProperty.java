package com.example.admin.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


@Configuration
public class LoadProperty {
	
	@Value("${user.service.port}")
	private String servicePort;

	public String getServicePort() {
		return servicePort;
	}

	public void setServicePort(String servicePort) {
		this.servicePort = servicePort;
	}
	
}
