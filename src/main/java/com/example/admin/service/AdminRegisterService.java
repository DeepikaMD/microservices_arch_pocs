package com.example.admin.service;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;

import com.example.admin.entity.AdminEntity;


@Service
public interface AdminRegisterService {
	public String registerAdmin(AdminEntity admin);
	public String retirveUser(String jobrole)throws JSONException ;
	
	
}
