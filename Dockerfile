FROM openjdk:8
MAINTAINER Deepika Muralidharan
WORKDIR /Admin
COPY . /Admin
CMD java -jar admin-0.0.1-SNAPSHOT.jar
EXPOSE 9090